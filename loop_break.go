package main

import "fmt"

func main() {
	for num := 0; num <= 100; num++ {
		if num%3 == 0 && num%5 == 0 {
			fmt.Printf(" First Positive Number divisible by both 3 and 5 is %d\n", num)
			break
		}
	}
}
