package main

import "fmt"

func main() {
	hasil_calculator(2, 4)
}

func pertambahan(a int, b int) {
	hasil := a + b
	fmt.Println(hasil)
}

func perkalian(angka_satu int, angka_dua int) {
	hasil := angka_satu * angka_dua
	fmt.Println(hasil)

}

func hasil_calculator(a int, b int) {
	pertambahan(a, b)
	perkalian(a, b)
}

// func hasil_calculator(a int, b int) {
// 	hasil_tambah := a + b
// 	hasil_kali := a * b
// 	fmt.Println(hasil_tambah)
// 	fmt.Println(hasil_kali)
// }
