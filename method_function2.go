package main

import (
	"fmt"
	"math"
)

type ArithmeticProgression struct {
	A, D float64
}

func (ap ArithmeticProgression) NthTerm(n int) float64 {
	return ap.A + float64(n-1)*ap.D
}

type GeometricProgression struct {
	A, R float64
}

func (gp GeometricProgression) NthTerm(n int) float64 {
	return gp.A * math.Pow(gp.R, float64(n-1))
}

func main() {
	ap := ArithmeticProgression{1, 2}
	gp := GeometricProgression{1, 2}

	fmt.Println("5th term of the Aritmatics series =", ap.NthTerm(5))
	fmt.Println("5th term of the Geometric series =", gp.NthTerm(5))
}
