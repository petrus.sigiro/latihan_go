package main

import "fmt"

func main() {

	var number = make([]int, 3, 3)
	fmt.Println(len(number))
	fmt.Println(cap(number))

	number = append(number, 20)
	fmt.Println(len(number))
	fmt.Println(cap(number))

	number = append(number, 40, 66)
	fmt.Println(len(number))
	fmt.Println(cap(number))

	number = append(number, 20)
	fmt.Println(len(number))
	fmt.Println(cap(number))
}
