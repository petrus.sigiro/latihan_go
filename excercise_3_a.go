package main

import "fmt"

func main() {
	pelajaran := make(map[string]int)

	pelajaran["math"] = 78
	pelajaran["biology"] = 80
	pelajaran["geography"] = 67
	pelajaran["physics"] = 70

	total := 0

	// fmt.Println(pelajaran)

	// check pelajjaran
	_, is_pelajaran := pelajaran["history"]
	// fmt.Println(is_pelajaran)

	if is_pelajaran == false {
		// tambah pelajaran
		pelajaran["history"] = 90

	}

	// fmt.Println(pelajaran)

	//Cek mata pelajaran lebih besar 80

	for key := range pelajaran {
		// fmt.Println(pelajaran[key])
		if pelajaran[key] >= 80 {
			fmt.Println("mata pelajaran lebih besar 80 :", key, pelajaran[key])
		}
	}

	fmt.Println("=================================")
	delete(pelajaran, "geography")
	// fmt.Println(pelajaran)

	fmt.Println(len(pelajaran))

	for val := range pelajaran {
		total += pelajaran[val]
	}

	fmt.Println(total / len(pelajaran))
}
