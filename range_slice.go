package main

import "fmt"

func main() {

	arr := []string{"aa", "bb", "cc", "dd", "ee", "ff"} //
	for i, b := range arr {
		fmt.Println("index :", i)
		fmt.Println("value :", b)
		fmt.Println("lain-lain", arr[i])

		arr := []string{"aa", "bb", "cc", "dd", "ee", "ff"} //
		for a := range arr {
			fmt.Println("value :", arr[a])
		}
	}
}
