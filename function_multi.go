package main

import "fmt"

func main() {
	multi, add, minus := calculate(100)
	fmt.Println("multi:", multi, "add:", add, "minus:", minus)
	// fmt.Print(calculate(100))
}

func calculate(d int) (int, int, int) {
	multiply := d * 5
	add := d + 10
	minus := d - 10

	return multiply, add, minus
}
