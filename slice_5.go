package main

import "fmt"

func main() {

	a := make([]int, 5)
	a[0] = 4
	a[1] = 9
	a[2] = 8

	a = append(a, 65)

	b := make([]int, len(a))
	copy(b, a)

	// b := a
	b[0] = 2
	fmt.Println(" a is", a)
	fmt.Println(" b is", b)
}
