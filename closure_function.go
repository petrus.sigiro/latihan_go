package main

import "fmt"

func main() {
	var getMin = func(n []int) int {
		var min int
		for i, e := range n {
			if i == 0 {
				min = e
			} else if e < min {
				min = e
			}

		}
		return min
	}
	var numbers = []int{2, 3, 4, 3, 4, 2, 3}
	var min = getMin(numbers)
	fmt.Printf("data :  %v\nmin : %v\n", numbers, min)
}
