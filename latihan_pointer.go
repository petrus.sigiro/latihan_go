package main

import "fmt"

func main() {
	ptr := new(int)
	*ptr = 100

	fmt.Println("ptr = ", ptr, "ptr value =", *ptr)
}
