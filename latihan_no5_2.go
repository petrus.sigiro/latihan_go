package main

import "fmt"

func main() {

	var a [10]int

	a[0] = 1
	a[1] = 3
	a[2] = 5
	a[3] = 7
	a[4] = 9
	a[5] = 2
	a[6] = 4
	a[7] = 6
	a[8] = 8
	a[9] = 10
	a[10] = 11

	fmt.Println(a[0], a[1], a[2], a[3], a[4], " are odd")
	fmt.Println(a[5], a[6], a[7], a[8], a[9], " are even")
	fmt.Println(a[10])
}
