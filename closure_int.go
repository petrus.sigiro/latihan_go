package main

import "fmt"

func main() {
	var numbers = []int{2, 3, 4, 3, 4, 2, 3}
	func(n []int) {
		var min int
		for i, e := range n {
			if i == 0 {
				min = e
			} else if e < min {
				min = e
			}

		}
		fmt.Println(min, n)
	}(numbers)

}
