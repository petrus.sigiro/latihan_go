package main

import "fmt"

func main() {

	var a [7]int

	// fmt.Println(a)

	a[0] = 10
	a[1] = 8
	a[2] = 12
	a[5] = 77

	// fmt.Println(a)

	fmt.Println(a[2])

}
