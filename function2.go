package main

import "fmt"

func main() {
	menit, detik := jamKeDetik(5)
	fmt.Println("menit :", menit, "detik:", detik)
}

func jamKeDetik(d float64) (float64, float64) {

	menit := d / 10
	detik := d / 60 / 60

	return menit, detik

}
