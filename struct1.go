package main

import "fmt"

type Person struct {
	FirstName string
	LastName  string
	Age       int
}

func main() {
	p1 := Person{"Martha", "Sutopo", 17}
	fmt.Println("personal`:", p1.FirstName)

	p2 := &p1

	fmt.Println(p2)
	fmt.Println(p2.LastName)
	fmt.Println((*p2).LastName)
}
