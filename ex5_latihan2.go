package main

import "fmt"

type Person struct {
	FirstName string
	LastName  string
	Job       string
	Salary    int
}

func (s Person) getFirstname() string {
	return s.FirstName
}
func (s Person) getLastname() string {
	return s.LastName
}

func (s Person) getJob() string {
	return s.Job
}

func (s Person) getSalary() int {
	return s.Salary
}

func main() {

	s := Person{"budi", "pratama", "marketing", 2000000}
	fmt.Println("firsname :", s.FirstName)
	fmt.Println("lastname :", s.LastName)
	fmt.Println("job :", s.Job)
	fmt.Println("salary :", s.Salary)
}
