package main

import "fmt"

type Point struct {
	X, Y float64
}

func (p Point) IsAbove(y float64) bool {
	return p.Y > y
}

func main() {
	a := Point{2.0, 4.0}
	// IsAbove := 1.0

	fmt.Println("Point :", a)
	fmt.Println("Is Point p Located above the line y =1.0 ?:", a.IsAbove(1))
}
