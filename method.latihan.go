package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y float64
}

func (v Point) Abs() {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func main() {
	v := Point{3, 4}
	fmt.Println(v.Abs())
}
