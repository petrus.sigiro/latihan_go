package main

import "fmt"

func main() {

	func(msg int) {
		fmt.Println(msg)
	}(12345)

}
