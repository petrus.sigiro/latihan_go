package main

import "fmt"

func main() {

	for a := 0; a < 5; a++ {
		for b := 5; b > 0; b-- {
			fmt.Print(b)
		}
		fmt.Println("")
	}

}
