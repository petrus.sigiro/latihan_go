package main

import "fmt"
import "encoding/json"

type User struct {
	FullName string `json:"Name"`
	Age      int
}

func main() {
	var jsonString = `[
		{"Name": "Budi Pratama", "Age":34},
		{"Name": "Jimmy Hendrawan", "Age":32}
		]`

	var data []User

	var err = json.Unmarshal([]byte(jsonString), &data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(" User 1 :", data[0].FullName)
	fmt.Println(" User 2 :", data[1].FullName)
}
