package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var jsonString = `{"Name" : "Budi Pratama", "Age": 34}`
	var jsonData = []byte(jsonString)

	var data1 map[string]interface{}
	json.Unmarshal(jsonData, &data1)

	fmt.Println("User:", data1["Name"])
	fmt.Println("Age:", data1["Age"])

	var data2 interface{}
	json.Unmarshal(jsonData, &data2)

	var decodeData = data2.(map[string]interface{})
	fmt.Println("User:", decodeData["Name"])
	fmt.Println("Age:", decodeData["Age"])

}
