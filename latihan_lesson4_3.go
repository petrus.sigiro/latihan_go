package main

import "fmt"

func main() {
	number := []int{21, 31, 41}
	multi := calculate(number...)
	fmt.Println(multi)
}

func calculate(numbers ...int) []int {
	var perkalian int = 2
	a := make([]int, 0)
	for _, calculate := range numbers {
		// fmt.Println(calculate * perkalian)

		a = append(a, calculate*perkalian)

	}

	return a
}
