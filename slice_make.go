package main

import "fmt"

func main() {

	i := make([]int, 5)
	// fmt.Println(i)

	i[0] = 3
	i[1] = 5
	i[2] = 1
	i[3] = 10
	i[4] = 32
	// fmt.Println(i)
	i = append(i, 88)
	fmt.Println(i)
	fmt.Println(i[1], i[4])
	fmt.Println(i[:3])
	fmt.Println(i[2:])
}
