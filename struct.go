package main

import "fmt"

type Person struct {
	FirstName string
	LastName  string
	Age       int
}

func main() {
	var p Person
	fmt.Println(p)

	p1 := Person{"Petrus", "Sigiro", 17}
	fmt.Println("Person1:", p1.Age)

	p2 := Person{
		FirstName: "budi",
		LastName:  "hari",
		Age:       32,
	}
	fmt.Println("Person2 :", p2)

	p3 := Person{FirstName: "siska"}
	fmt.Println("person3", p3)
}
