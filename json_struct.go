package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Fullname string
	Age      int
}

func main() {

	var jsonString = `{"Name": "Budi Pratama", "Age": 34}`
	var jsonData = []byte(jsonString)
	fmt.Println(jsonData)

	var data User

	var err = json.Unmarshal(jsonData, &data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// fmt.Println("user :", data.Fullname)
	// fmt.Println("age :", data.Age)

}
