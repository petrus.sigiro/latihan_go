package main

import (
	"fmt"
	"time"
)

func main() {
	a := time.Now()
	switch {
	case a.Hour() > 12:
		fmt.Println(" it's after noon")
	case a.Hour() < 12:
		fmt.Println(" it's before noon")

	}
}
