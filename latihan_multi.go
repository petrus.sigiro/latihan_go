package main

import "fmt"

func main() {
	multi, add, minus := hasil(15)
	fmt.Println("multi :", multi, "add :", add, "minus :", minus)
}

func hasil(a int) (int, int, int) {
	multi := a * 12
	add := a + 12
	minus := a - 12

	return multi, add, minus
}
