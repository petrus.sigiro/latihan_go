package main

import "fmt"

func main() {

	a := [...]int{5, 4, 90, 12, 34}
	b := a
	b[0] = 2
	fmt.Println(" a is ", a)
	fmt.Println(" b is ", b)

}
