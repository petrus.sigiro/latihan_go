package main

import "fmt"

func main() {
	a := [...]int{12, 78, 50, 65, 34}

	fmt.Println(len(a))

	b := [3]int{12}
	fmt.Println(b)
}
