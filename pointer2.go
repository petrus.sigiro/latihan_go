package main

import "fmt"

func main() {
	var a string = "a"
	var ip *string

	ip = &a

	fmt.Println(&a)
	fmt.Println(ip)
	fmt.Println(*ip)

}
