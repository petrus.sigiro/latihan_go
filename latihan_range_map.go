package main

import "fmt"

func main() {
	var a = map[string]int{
		"aa": 22,
		"bb": 33,
		"cc": 44,
		"dd": 55,
		"ee": 66,
		"ff": 77}

	delete(a, "aa")

	a["aa"] = 22

	for key, val := range a {
		fmt.Println(key, "\t:", val)

	}

}
