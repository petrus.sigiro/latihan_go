package main

import "fmt"

func main() {

	keliling_persegi1(4)
	luas_persegi1(4)

	fmt.Println("==================================================")

	fmt.Println(keliling_persegi(4))
	fmt.Println(luas_persegi(4))
}

func keliling_persegi(a int) int {
	hasil := 4 * a
	return hasil

}

func luas_persegi(a int) int {
	hasil := a * a
	return hasil
}

func keliling_persegi1(a int) {
	hasil := 4 * a
	fmt.Println(hasil)

}

func luas_persegi1(a int) {
	hasil := a * a
	fmt.Println(hasil)
}
