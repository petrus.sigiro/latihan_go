package main

import "fmt"

type Person struct {
	FirstName string
	LastName  string
	Job       string
	Salary    int
}

func main() {

	p1 := Person{"Budi", "Pratama", "Marketing", 2000000}
	fmt.Println("personal :", p1)
}
