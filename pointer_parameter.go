package main

import "fmt"

func main() {
	var number = 9
	fmt.Println("before :", number)

	change(&number, 15)
	fmt.Println("after :", number)
}

func change(original *int, value int) {
	*original = value

}
