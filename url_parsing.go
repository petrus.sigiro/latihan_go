package main

import (
	"fmt"
	"net/url"
)

func main() {
	var urlString = "https://www.bukalapak.com/tiket-pesawat?source=navbar&from=navbar_categories"
	var u, e = url.Parse(urlString)
	if e != nil {
		fmt.Println(e.Error())
		return
	}

	fmt.Printf("url : %s\n", urlString)

	fmt.Printf("protocol : %s/n", u.Scheme)
	fmt.Printf("host : %s/n", u.Host)
	fmt.Printf("path : %s/n", u.Path)

	// var name = u.Query()["source"][0]
	// var age = u.Query()["from"][0]
	// fmt.Printf("name :%s, age: %s/n", name, age)
}
