package main

import (
	"fmt"
)

type hitung interface {
	luas(a int) int
	// keliling() float64
}

type persegi struct {
	sisi int
}

func (p persegi) luas(a int) int {
	return (p.sisi * a)
}

// func (p persegi) keliling() float64 {
// 	return p.sisi * 4
// }

func main() {
	var bangunDatar hitung

	bangunDatar = persegi{10}
	fmt.Println("===== persegi")
	fmt.Println("Luas :", bangunDatar.luas(2))
	// fmt.Println("Keliling :", bangunDatar.keliling())

}
